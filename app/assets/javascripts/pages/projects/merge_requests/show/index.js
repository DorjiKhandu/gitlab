import initNotesApp from '~/mr_notes/init_notes';
import { initReportAbuse } from '~/projects/merge_requests';
import { initMrPage } from '../page';

initMrPage();
initNotesApp();
initReportAbuse();
